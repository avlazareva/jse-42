package ru.t1.lazareva.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.service.IServiceLocator;
import ru.t1.lazareva.tm.dto.model.SessionDTO;
import ru.t1.lazareva.tm.dto.request.AbstractUserRequest;
import ru.t1.lazareva.tm.enumerated.Role;
import ru.t1.lazareva.tm.exception.EndpointException;
import ru.t1.lazareva.tm.exception.user.AccessDeniedException;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected SessionDTO check(
            @Nullable final AbstractUserRequest request,
            @Nullable final Role role
    ) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        try {
            @NotNull final SessionDTO session = serviceLocator.getAuthService().validateToken(token);
            if (session.getRole() == null) throw new AccessDeniedException();
            if (!session.getRole().equals(role)) throw new AccessDeniedException();
            return session;
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        try {
            return serviceLocator.getAuthService().validateToken(token);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
    }

}
