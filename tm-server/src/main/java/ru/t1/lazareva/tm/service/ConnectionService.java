package ru.t1.lazareva.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.mybatis.caches.hazelcast.LoggingHazelcastCache;
import ru.t1.lazareva.tm.api.repository.IProjectRepository;
import ru.t1.lazareva.tm.api.repository.ISessionRepository;
import ru.t1.lazareva.tm.api.repository.ITaskRepository;
import ru.t1.lazareva.tm.api.repository.IUserRepository;
import ru.t1.lazareva.tm.api.service.IConnectionService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.model.ProjectDTO;
import ru.t1.lazareva.tm.dto.model.SessionDTO;
import ru.t1.lazareva.tm.dto.model.TaskDTO;
import ru.t1.lazareva.tm.dto.model.UserDTO;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.Session;
import ru.t1.lazareva.tm.model.Task;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;


    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
        this.entityManagerFactory = getEMFactory();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    @NotNull
    @SneakyThrows
    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        if (propertyService.getDBCache().equals("true")) {
            configuration.setCacheEnabled(true);
            configuration.addCache(new LoggingHazelcastCache("tm"));
        }
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

    @NotNull
    @Override
    public EntityManagerFactory getEMFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDBDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDBUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDBUser());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDBPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDBDialect());
        settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, "true");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDBShowSQL());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
