package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    @NotNull
    SessionDTO add(@NotNull final SessionDTO model) throws Exception;

    @NotNull
    SessionDTO add(@Nullable final String userId, @NotNull final SessionDTO model) throws Exception;

    void clear(@Nullable final String userId) throws Exception;

    boolean existsById(@Nullable final String userId, @Nullable final String id) throws Exception;

    int getSize(@Nullable final String userId) throws Exception;

    @Nullable
    List<SessionDTO> findAll(@Nullable final String userId) throws Exception;

    @Nullable
    SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) throws Exception;

    @Nullable
    SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception;

    void remove(@Nullable final String userId, @Nullable final SessionDTO model) throws Exception;

    void removeById(@Nullable final String userId, @Nullable final String id) throws Exception;

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception;

}
