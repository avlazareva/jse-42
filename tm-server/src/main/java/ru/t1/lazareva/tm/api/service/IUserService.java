package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDTO;
import ru.t1.lazareva.tm.enumerated.Role;

import java.util.List;

public interface IUserService {

    @NotNull
    UserDTO add(@NotNull final UserDTO model) throws Exception;

    void clear() throws Exception;

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception;

    @NotNull
    UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception;

    boolean existsById(@Nullable final String id) throws Exception;

    int getSize() throws Exception;

    @Nullable
    List<UserDTO> findAll() throws Exception;

    @Nullable
    UserDTO findOneById(@Nullable final String id) throws Exception;

    @Nullable
    UserDTO findOneByIndex(@Nullable final Integer index) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws Exception;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws Exception;

    Boolean isLoginExists(@Nullable final String login) throws Exception;

    Boolean isEmailExists(@Nullable final String email) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void remove(@Nullable final UserDTO model) throws Exception;

    void removeById(@Nullable final String id) throws Exception;

    void removeByIndex(@Nullable final Integer index) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    @NotNull
    UserDTO setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    @NotNull
    UserDTO updateUser(@Nullable final String id, @Nullable final String firstName, @Nullable final String lastName, @Nullable final String middleName) throws Exception;

}