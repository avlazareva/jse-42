package ru.t1.lazareva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<TaskDTO> {

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description);

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name);

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}