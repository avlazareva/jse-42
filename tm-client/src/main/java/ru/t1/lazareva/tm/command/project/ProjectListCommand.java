package ru.t1.lazareva.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDTO;
import ru.t1.lazareva.tm.dto.request.ProjectListRequest;
import ru.t1.lazareva.tm.dto.response.ProjectListResponse;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSortType(sort);
        @Nullable final ProjectListResponse response = getProjectEndpoint().listProject(request);
        if (response.getProjects() == null) response.setProjects(Collections.emptyList());
        @NotNull final List<ProjectDTO> projects = response.getProjects();
        renderProjects(projects);
    }

    private void renderProjects(@NotNull final List<ProjectDTO> projects) {
        int index = 1;
        for (@Nullable final ProjectDTO project : projects) {
            if (project == null) continue;
            System.out.println(index + ". " + project.getName());
            index++;
        }
    }

}