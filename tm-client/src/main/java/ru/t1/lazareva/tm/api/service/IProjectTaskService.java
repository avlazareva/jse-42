package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDTO;

public interface IProjectTaskService {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO bindTaskToTasProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

}