package ru.t1.lazareva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.command.AbstractCommand;
import ru.t1.lazareva.tm.dto.model.TaskDTO;
import ru.t1.lazareva.tm.enumerated.Status;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return getServiceLocator().getTaskEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(@NotNull final List<TaskDTO> tasks) {
        int index = 1;
        for (@Nullable TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}