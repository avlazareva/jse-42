package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDTO;
import ru.t1.lazareva.tm.enumerated.Role;

public interface IAuthService {

    void checkRoles(@Nullable Role[] roles);

    @NotNull
    UserDTO check(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO registry(@NotNull String login, @NotNull String password, @NotNull String email);

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean isAuth();

    @NotNull
    String getUserId();

    @NotNull
    UserDTO getUser();

}