package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDTO;
import ru.t1.lazareva.tm.enumerated.Status;

import java.util.List;

public interface ITaskService extends IUserOwnedService<TaskDTO> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull TaskDTO create(@Nullable String userId, @Nullable String name);

}