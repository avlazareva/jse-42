package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDTO;
import ru.t1.lazareva.tm.enumerated.Sort;
import ru.t1.lazareva.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IUserOwnedService<ProjectDTO> {

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    ProjectDTO changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    ProjectDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Comparator<ProjectDTO> comparator);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable Sort sort);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    ProjectDTO create(@Nullable String userId, @Nullable String name);

}