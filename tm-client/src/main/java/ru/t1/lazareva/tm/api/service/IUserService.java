package ru.t1.lazareva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.UserDTO;
import ru.t1.lazareva.tm.enumerated.Role;

public interface IUserService extends IService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    @Nullable
    UserDTO findByEmail(@Nullable String email);

    @Nullable
    @SuppressWarnings("UnusedReturnValue")
    UserDTO removeByLogin(@Nullable String login);

    @Nullable
    @SuppressWarnings("unused")
    UserDTO removeByEmail(@Nullable String email);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    UserDTO setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    @SuppressWarnings("UnusedReturnValue")
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}