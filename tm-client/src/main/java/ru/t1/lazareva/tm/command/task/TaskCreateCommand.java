package ru.t1.lazareva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.request.TaskCreateRequest;
import ru.t1.lazareva.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    private static final String NAME = "task-create";

    @NotNull
    private static final String DESCRIPTION = "Create new task.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASKS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(getToken());
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().createTask(request);
    }

}