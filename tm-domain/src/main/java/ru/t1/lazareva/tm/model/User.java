package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractModel {

    @NotNull
    @Column(nullable = false, name = "login")
    private String login;

    @NotNull
    @Column(nullable = false, name = "password_hash")
    private String passwordHash;

    @Nullable
    @Column(nullable = true)
    private String email;

    @Nullable
    @Column(nullable = true, name = "first_name")
    private String firstName;

    @Nullable
    @Column(nullable = true, name = "last_name")
    private String lastName;

    @Nullable
    @Column(nullable = true, name = "middle_name")
    private String middleName;

    @NotNull
    @Column(nullable = false, name = "role")
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false, name = "locked")
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user")
    private List<Session> sessions = new ArrayList<>();

}
