package ru.t1.lazareva.tm.exception.user;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}