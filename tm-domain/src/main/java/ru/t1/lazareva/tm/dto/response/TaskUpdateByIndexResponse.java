package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexResponse extends AbstractTaskResponse {

    public TaskUpdateByIndexResponse(@NotNull final TaskDTO task) {
        super(task);
    }

}
