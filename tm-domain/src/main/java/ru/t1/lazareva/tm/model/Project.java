package ru.t1.lazareva.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.api.model.IWBS;
import ru.t1.lazareva.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_project")
public final class Project extends AbstractUserOwnedModel implements IWBS {

    private static final long serialVersionUID = 1;

    @NotNull
    @Column(nullable = false, name = "name")
    private String name = "";

    @Nullable
    @Column(nullable = true, name = "description")
    private String description = "";

    @NotNull
    @Column(nullable = false, name = "status")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false, name = "created")
    private Date created = new Date();

    @NotNull
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @Override
    public String toString() {
        return this.getName() + " | Status: " + status.getDisplayName() + " |";
    }

}