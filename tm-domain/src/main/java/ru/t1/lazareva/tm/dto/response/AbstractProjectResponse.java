package ru.t1.lazareva.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDTO;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private ProjectDTO project;

    public AbstractProjectResponse(@Nullable final ProjectDTO project) {
        this.project = project;
    }
}
