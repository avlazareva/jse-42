package ru.t1.lazareva.tm.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

}
